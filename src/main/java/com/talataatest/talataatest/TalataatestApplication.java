package com.talataatest.talataatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TalataatestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TalataatestApplication.class, args);
	}

}
