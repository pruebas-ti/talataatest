package com.talataatest.talataatest.services;

import java.util.List;

import com.talataatest.talataatest.entity.Product;

public interface ProductService {

    public List<Product> findAll();

    public Product findById(Long id);
    
    public Product save(Product product);

    public Product update(Long id, Product product);
    
    public void delete(Long id);
}
