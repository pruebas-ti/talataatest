package com.talataatest.talataatest.services;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talataatest.talataatest.entity.Product;
import com.talataatest.talataatest.repository.ProductRepository;

import jakarta.transaction.Transactional;

@Service
public class ProductServiceImp implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    @Transactional
    public List<Product> findAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    @Transactional
    public Product findById(Long id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    @Transactional
    public Product update(Long id, Product product) {
        Product currentProduct = productRepository.findById(id).orElse(null);

        if(currentProduct == null){
            return null;
        }

        if(Objects.nonNull(product.getName()) && !"".equalsIgnoreCase(product.getName()))
            currentProduct.setName(product.getName());
        
        if(Objects.nonNull(product.getDescription()) && !"".equalsIgnoreCase(product.getDescription()))
        currentProduct.setDescription(product.getDescription());
        
        if(Objects.nonNull(product.getUpdateAt()))
            currentProduct.setUpdateAt(product.getUpdateAt());
        
        if(Objects.nonNull(product.getValue()))
            currentProduct.setValue(product.getValue());

        return productRepository.save(currentProduct);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        productRepository.deleteById(id);
    }

}
