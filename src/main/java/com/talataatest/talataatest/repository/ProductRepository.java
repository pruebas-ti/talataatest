package com.talataatest.talataatest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.talataatest.talataatest.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    
}
