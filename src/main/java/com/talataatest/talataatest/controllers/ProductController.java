package com.talataatest.talataatest.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.talataatest.talataatest.entity.Product;
import com.talataatest.talataatest.services.ProductService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;



@RestController
@RequestMapping("/api/v1")
public class ProductController {
    
    @Autowired
    private ProductService productService;

    private Map<String, Object> map = new HashMap<String, Object>();

    @GetMapping("/products")
    public ResponseEntity<Object> getProducts(@RequestParam String param) {
        try {
            List<Product> products = productService.findAll();
            return new ResponseEntity<Object>(products, HttpStatus.OK);
        } catch (Exception e) {
            map.put("message", e.getMessage());
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable Long id) {
        try {
            Product product = productService.findById(id);
            return new ResponseEntity<Object>(product, HttpStatus.OK);
        } catch (Exception e) {
            map.put("message", e.getMessage());
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping("/products")
    public ResponseEntity<Object> postNewProduct(@RequestBody Product product) {
        try {
            Product response = productService.save(product);
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } catch (Exception e) {
            map.put("message", e.getMessage());
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("products/{id}")
    public ResponseEntity<Object> putUpdataProduct(@PathVariable Long id, @RequestBody Product product) {
        try {
            Product response = productService.update(id, product);
            return new ResponseEntity<Object>(response, HttpStatus.OK);
        } catch (Exception e) {
            map.put("message", e.getMessage());
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("products/{id}")
    ResponseEntity<Object> putUpdataProduct(@PathVariable Long id) {
        try {
            productService.delete(id);
            return new ResponseEntity<Object>("delete ok", HttpStatus.OK);
        } catch (Exception e) {
            map.put("message", e.getMessage());
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
